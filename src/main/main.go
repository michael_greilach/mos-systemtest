package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jprobinson/eazye"
	"net/http"
	"regexp"
)

var (
	apiUri string = "http://digitack.dnshome.de:8080/mos/rest"
	
	webUri string = "http://digitack.dnshome.de"

	loginData SuccesfulLoginData

	guestId string

	//mail info for test@digitack.de to verify a newly created useraccount
	mailInfo = eazye.MailboxInfo{
		Host:   "mail.digitack.de",
		TLS:    true,
		User:   "delphintest@digitack.de",
		Pwd:    "%w3d,+aKJ796RAWG",
		Folder: "INBOX",
	}
	
	serverMailFrom string = "noreply@digitack.de"
)

func buildURI(res string) string {

	return fmt.Sprint(apiUri, res)

}

//log a user specified by username and password in
func login(username, password string) SuccesfulLoginData {

	fmt.Println("\tPOST /user/login: ")
	fmt.Println("\t\tLogging in User: ", username)

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(LoginData{username, password, 14, 1})

	req, _ := http.NewRequest(http.MethodPost, buildURI("/user/login"), b)
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{}

	resp, err := client.Do(req)

	defer resp.Body.Close()

	if err != nil {
		fmt.Println("\t\tNo Connection.")
		panic("No Connection")
	}

	switch {

	case resp.StatusCode == 200:
		fmt.Println("\t\tLogin successful.")

		var success SuccesfulLoginData
		if err := json.NewDecoder(resp.Body).Decode(&success); err != nil {
			panic("Decoder Error")
		}

		fmt.Println("\t\t", success)
		return success

	case resp.StatusCode >= 400 && resp.StatusCode < 500:

		var erro Error
		if err := json.NewDecoder(resp.Body).Decode(&erro); err != nil {
			panic("Decoder Error")
		}
		fmt.Println("\t\tLogin not successful.")
		fmt.Println(erro.ErrorCode + " " + erro.Message)

		return SuccesfulLoginData{}
	default:
		panic("No valid return code.")

	}
}

//requests a guest-login
func loginGuest() string {

	fmt.Println("\tPOST /user/login/guest: ")
	fmt.Println("\t\tLogging in Guest.")

	req, err := http.NewRequest(http.MethodGet, buildURI("/user/login/guest"), nil)

	if err != nil {
		panic("Request Building Error")
	}

	client := http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		panic("No Connection")
	}

	defer resp.Body.Close()

	switch {
	case (resp.StatusCode >= 200) && (resp.StatusCode < 300):

		var sessionId SessionId
		if err := json.NewDecoder(resp.Body).Decode(&sessionId); err != nil {
			panic("Decoder Error")
		}

		fmt.Println("\t\tLogin successful. sessionId : ", sessionId.SessionId)
		return sessionId.SessionId
	case (resp.StatusCode >= 400) && (resp.StatusCode < 500):

		var erro Error
		if err := json.NewDecoder(resp.Body).Decode(&erro); err != nil {
			panic("Decoder Error")
		}

		panic(erro.ErrorCode + " " + erro.Message)
	default:
		panic("Panic")
	}

}

//Method testing mos resource GET '/user{userId}'
func requestUserDetails(userId, sessionId string) UserData {

	fmt.Println("\tGET /user/", userId, ": ")
	fmt.Println("\t\tFetching Userdetails for User with id ", userId)

	req, err := http.NewRequest(http.MethodGet, buildURI(fmt.Sprint("/user/"+userId+"?sessionId="+sessionId)), nil)

	if err != nil {
		panic("Request Building Error")

	}

	client := http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		panic("No Connection")
	}

	defer resp.Body.Close()
	switch {
	case (resp.StatusCode >= 200) && (resp.StatusCode < 300):

		var ud UserData
		if err := json.NewDecoder(resp.Body).Decode(&ud); err != nil {
			panic("Decoder Error")
		}

		return ud
	case (resp.StatusCode >= 400) && (resp.StatusCode < 500):

		var erro Error
		if err := json.NewDecoder(resp.Body).Decode(&erro); err != nil {
			panic("Decoder Error")
		}

		panic(erro.ErrorCode + " " + erro.Message)
	default:
		panic("Panic")
	}

}

//Tests POST /user
func createUser(username, password, email string, newsletter bool) string {

	fmt.Println("\tPOST /user/:")
	fmt.Println("\t\tCreating new User (", username, ") with email (", email, ")")

	contact := ContactData{EmailAdress: &email}

	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(RegUser{username, password, contact, newsletter, false})

	if err != nil {
		panic("Request Building Error")

	}
	req, _ := http.NewRequest(http.MethodPost, buildURI("/user"), b)
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{}

	resp, err := client.Do(req)

	defer resp.Body.Close()

	switch {
	case (resp.StatusCode >= 200) && (resp.StatusCode < 300):

		var uid UserId
		if err := json.NewDecoder(resp.Body).Decode(&uid); err != nil {
			panic("Decoder Error")
		}

		return uid.UserId
	case (resp.StatusCode >= 400) && (resp.StatusCode < 500):

		var erro Error
		if err := json.NewDecoder(resp.Body).Decode(&erro); err != nil {
			panic("Error while decoding Error Message")
		}

		panic(erro.ErrorCode + " " + erro.Message)
	default:
		panic("Panic")
	}

}

//creates a new user and verifies the account
func testUserCreationAndVerification(username, password, email string) {

	//create a new user
	createUser(username, password, email, false)

	//retrieve mails
	fmt.Println("\t\tRetrieving Mail for test@digitack.de")

	mailList, ok := eazye.GetUnread(mailInfo, true, true)

	if ok != nil {
		fmt.Println(ok.Error())
	}

	fmt.Println("\t\tRetrieved ", len(mailList), "Mails from the Server.\n")
	fmt.Println("\t\tStart iteration..")

	//iterate over all retrieved mails
	for i, mail := range mailList {

		fmt.Println("\t\tMail ", (i + 1), " - from "+mail.From.Address)

		//only acknowledge mails from "System@myfoodhero.net"
		if mail.From.Address == serverMailFrom {

			fmt.Println("\t\t\tRetrieving verification token..")

			//get the html-part of the mail
			htmlpart := string(mail.HTML)

			r, _ := regexp.Compile("/mos/rest/email/verify")
			index := r.FindStringIndex(htmlpart)

			i := index[0]

			//search the token-string, ending with "
			for {

				i++

				if string(htmlpart[i]) == "\"" {

					break
				}
			}

			//extract the token
			token := htmlpart[index[1]:i]

			fmt.Println("\t\t\tToken found. Accessing verification Service.")

			//and request verification from the server
			fmt.Println("\t\tGET /email/verify", token)
			req, _ := http.NewRequest(http.MethodGet, buildURI("/email/verify"+token), nil)
			client := http.Client{}
			resp, _ := client.Do(req)

			defer resp.Body.Close()

			if resp.StatusCode != 200 {
				panic("Verification not successful.")
			}

			fmt.Println("\t\t\t", resp.StatusCode, "Verification successful.")

			break
		} else {
			fmt.Println("\t\t\tSkipping..")
		}
	}

}

//test creation of duplicate username
func testCreateUserUsernameAlreadyTaken() {

	//defer recovery from the panic
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("\t\tPANIC: ", r)
		}
	}()

	//first create new user testiXX
	createUser("testiXX", "Testers3n", "test2@digitack.de", false)

	//second create the user again but with another valid email
	createUser("testiXX", "Testers3n", "test3@digitack.de", false)

}

//test creation of account which already exists
func testCreateUserEmailAlreadyTaken() {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("\t\tPANIC: ", r)
		}
	}()

	//assuming admin is present with kontakt@myfoodhero.net as mail - create a new account with that mail
	createUser("testiXXX", "Testers3n", "kontakt@myfoodhero.net", false)

}

//deletes a verified user
func testDeleteUser(username, password, email string, userData SuccesfulLoginData) {

	fmt.Println("\tDELETE /user/", userData.UserId, ":")

	req, err := http.NewRequest(http.MethodDelete, buildURI(fmt.Sprint("/user/"+userData.UserId+"?sessionId="+userData.SessionId)), nil)

	if err != nil {
		panic("Request Building Error")

	}

	client := http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		panic("No Connection")
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		panic("Deletion failed")
	}

	//now check the emails
	mailList, ok := eazye.GetUnread(mailInfo, true, true)

	if ok != nil {
		fmt.Println(ok.Error())
	}

	fmt.Println("\t\tRetrieved ", len(mailList), "Mails from the Server.\n")
	fmt.Println("\t\tStart iteration..")

	//iterate over all retrieved mails
	for i, mail := range mailList {

		fmt.Println("\t\tMail ", (i + 1), " - from "+mail.From.Address)

		//only acknowledge mails from "System@myfoodhero.net"
		if mail.From.Address == serverMailFrom {

			fmt.Println("\t\t\tRetrieving verification token..")

			//get the html-part of the mail
			htmlpart := string(mail.HTML)

			r, _ := regexp.Compile("/mos/rest/email/deleteAccount")
			index := r.FindStringIndex(htmlpart)

			i := index[0]

			//search the token-string, ending with "
			for {

				i++

				if string(htmlpart[i]) == "\"" {

					break
				}
			}

			//extract the token
			token := htmlpart[index[1]:i]

			fmt.Println("\t\t\tToken found. Accessing deletion Service.")

			//now use the token to delete the account
			fmt.Println("\t\tGET /email/deleteAccount", token)
			req, _ := http.NewRequest(http.MethodGet, buildURI("/email/deleteAccount"+token), nil)
			client := http.Client{}
			resp, _ := client.Do(req)

			defer resp.Body.Close()

			if resp.StatusCode != 200 {
				panic("Deletion not successful.")
			}

			fmt.Println("\t\t\t", resp.StatusCode, "Account deletion successful.")

			break
		} else {
			fmt.Println("\t\t\tSkipping..")
		}
	}
}

//requests a passwordchange for the given userid, and the oldpassword / newpassword combination
func testUserPasswordChange(userId, sessionId, oldPassword, newPassword string) {

	fmt.Println("\tPUT /user/", userId, "/pwdchange")
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(PasswordChangeData{OldPassword: oldPassword, NewPassword: newPassword})

	req, _ := http.NewRequest(http.MethodPut, buildURI("/user/"+userId+"/pwdchange?sessionId="+sessionId), b)
	req.Header.Set("Content-Type", "application/json")

	fmt.Println("\tRequesting Passwordchange from ", oldPassword, " to ", newPassword)
	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		fmt.Println("\t", resp.StatusCode, " Change Successful.")

	} else {
		fmt.Println("\t", resp.StatusCode, "Change not succesful.")
	}
}

func testUserForgotPassword(email, newPassword string) {

	fmt.Println("\tGET /email/changePasswordRequest?email=", email)

	req, _ := http.NewRequest(http.MethodGet, buildURI("/email/changePasswordRequest?email="+email), nil)

	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()

	//now check the emails
	mailList, ok := eazye.GetUnread(mailInfo, true, true)

	if ok != nil {
		fmt.Println(ok.Error())
	}

	fmt.Println("\t\tRetrieved ", len(mailList), "Mails from the Server.\n")
	fmt.Println("\t\tStart iteration..")

	//iterate over all retrieved mails
	for i, mail := range mailList {

		fmt.Println("\t\tMail ", (i + 1), " - from "+mail.From.Address)

		//only acknowledge mails from "System@myfoodhero.net"
		if mail.From.Address == serverMailFrom {

			fmt.Println("\t\t\tRetrieving verification token..")

			//get the html-part of the mail
			htmlpart := string(mail.HTML)

			r, _ := regexp.Compile(webUri + "/")
			index := r.FindStringIndex(htmlpart)

			i := index[0]

			//search the token-string, ending with "
			for {

				i++

				if string(htmlpart[i]) == "\"" {

					break
				}
			}

			//extract the token
			token := htmlpart[index[1]:i]

			//prepare data to post
			b := new(bytes.Buffer)
			json.NewEncoder(b).Encode(PasswordForgottenData{Token: token, Password: newPassword})

			//use the token to access the passwordchange
			req, _ := http.NewRequest(http.MethodPost, buildURI("/email/changePassword?email="+email), b)
			req.Header.Set("Content-Type", "application/json")

			client := http.Client{}
			resp, _ := client.Do(req)

			defer resp.Body.Close()

			//check the statuscode
			if resp.StatusCode != 200 {
				fmt.Println("\t\t", resp.StatusCode, "Password change not succesful")
			} else {
				fmt.Println("\t\t", resp.StatusCode, "Password change succesful")
			}

			break
		} else {
			fmt.Println("\t\t\tSkipping..")
		}
	}

}

//retrieves all produzcts from mos
func testGetAllProducts(sessionId string) []FoodProduct {

	fmt.Println("\tGET /products/")

	req, _ := http.NewRequest(http.MethodGet, buildURI("/products?sessionId="+sessionId), nil)

	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()

	//check the statuscode
	if resp.StatusCode != 200 {
		fmt.Println("\t\t", resp.StatusCode, "Request not succesful")
	} else {
		fmt.Println("\t\tRequest succesful")

		var fp []FoodProduct
		if err := json.NewDecoder(resp.Body).Decode(&fp); err != nil {
			panic("Decoder Error")
		}

		return fp

	}

	return []FoodProduct{}
}

//creates a new foodproduct with the specified parameters
func testCreateProduct(sessionId, gtin, caption, description, foodCategory, producer, brandname, currency string, priceTag float64, flags int32) {
	
	fmt.Println("\tPOST /products/")
	
	b := new(bytes.Buffer)
	fp := NewFoodProduct{
						GtinCode : gtin, 
						Caption: caption, 
						Description : description, 
						PriceTag :priceTag, 
						Currency: currency,
						Producer : producer, 
						BrandName : brandname,
						FoodCategory : foodCategory, 
						Flags: flags }
	json.NewEncoder(b).Encode(fp)
	
	req, _ := http.NewRequest(http.MethodPost, buildURI("/products?sessionId="+sessionId), b)
	req.Header.Set("Content-Type", "application/json")
	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()

	//check the statuscode
	if resp.StatusCode != 201 && resp.StatusCode != 200 {
		fmt.Println("\t\t", resp.StatusCode, "Request not succesful")
	} else {
		fmt.Println("\t\t201 Request succesful")

	}
	
}

func testDeleteProduct(sessionId, gtin string){
	
	fmt.Println("\tDELETE /products/" + gtin)
	
	req, _ := http.NewRequest(http.MethodDelete, buildURI("/products/" + gtin +"?sessionId="+sessionId), nil)

	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()

	//check the statuscode
	if resp.StatusCode != 200 {
		fmt.Println("\t\t", resp.StatusCode, "Request not succesful")
	} else{
		fmt.Println("\t\t200 Request succesful")
		
	}
}

func updateProductCaption(sessionId, gtin, newCaption, newDescription, newProducer, newBrandname, newFoodCategory, newCurrency string, newPriceTag float64, newFlags int32){
	
	fmt.Println("\tPUT /products/" + gtin)
	
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(NewFoodProduct{ 
										Caption: newCaption,
										Currency: newCurrency,
										Description: newDescription,
										PriceTag: newPriceTag,
										Producer: newProducer,
										BrandName: newBrandname,
										FoodCategory: newFoodCategory,
										Flags: newFlags,
										GtinCode: gtin})
										
	req, _ := http.NewRequest(http.MethodPut, buildURI("/products/" + gtin +"?sessionId="+sessionId), b)
	req.Header.Set("Content-Type", "application/json")
	
	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()

	//check the statuscode
	if resp.StatusCode != 200 {
		fmt.Println("\t\t", resp.StatusCode, "Request not succesful")
	} else{
		fmt.Println("\t\t200 Request succesful")
		
	}
}

//tests the UserInterface of mos
func testUserResource() {

	//variables fot the test
	var username = "testiXXX"
	var password = "Testers3n"
	var email = "delphintest@digitack.de"

	//test user creation and email verification
	fmt.Println("Create new User testiXXX:")
	testUserCreationAndVerification(username, password, email)

	//login testix
	fmt.Println("Logging in User testiX:")
	testiXData := login("testiXXX", "Testers3n")

	//test creation of an account whose email is already taken
	fmt.Println("Testing Account creation with Email already taken:")
	testCreateUserEmailAlreadyTaken()

	//test creation of an account whose username is already taken
	fmt.Println("Testing Account creation with Username already taken:")
	testCreateUserUsernameAlreadyTaken()

	//test passwordchange
	fmt.Println("Test Password-Change for testiXXX")
	testUserPasswordChange(testiXData.UserId, testiXData.SessionId, password, "Testers2n")

	//test password forgotten
	fmt.Println("Test Password forgotten for testiXXX")
	testUserForgotPassword("delphintest@digitack.de", "Testers1n")

	//deletes the user
	fmt.Println("Delete Account testiXXX")
	testDeleteUser(username, password, email, testiXData)
}

//retrieves all products
func testProductInterface() {

	//variables fot the test
	var username = "admin"
	var password = "Eg56V5Vx9fRR+PcKQlC7KNbnydkugo4ex1hywC9H64c="

	//login admin-user for easier tests
	fmt.Println("Login Admin for FoodProductUpdates")
	adminLogin := login(username, password)

	//retrieve all foodproducts
	fp := testGetAllProducts(adminLogin.SessionId)

	//output them all
	for i, fp := range fp {
		fmt.Println("\t\t", (i + 1), "'th Product: ", fp)

	}

	newProductGTIN := "22345678"
	
	//test creation of a new foodproduct	
	fmt.Println("Create new FoodProduct")
	testCreateProduct(adminLogin.SessionId, newProductGTIN, "MegaProduct", "beschreib mich", "FC_OTHER", "", "", "CUR_EURO", 1.2, 0)
	
	//update the roduct with new information
	fmt.Println("Update the newly created Product")
	updateProductCaption(adminLogin.SessionId, newProductGTIN, "New MeagaProduct", "describe me", "produces me", "brands me", "FC_BREAD_WHOLEGRAIN", "CUR_EURO", 6.4, 4)
	
	//and delete the created product with the admin-account
	fmt.Println("Delete the created Product")
	testDeleteProduct(adminLogin.SessionId, newProductGTIN)
	

}

//System- Tests for mos (myFoodHero Offer Service) - rest service.
func main() {

	//get guestlogin
	fmt.Println("---------------------------------------- Testing Logins ----------------------------------------")
	fmt.Println("Requesting Guest-Login:")
	guestId = loginGuest()

	//login a user
	fmt.Println("Logging in User testi:")
	loginData = login("testiX", "qIqtZRu/XUIUzF2bjcYFqqnBRuT2mDUHmlF4IbgEvws=")

	//request its user-details
	fmt.Println("Requesting testis UserDetails:")
	requestUserDetails(loginData.UserId, loginData.SessionId)

	//test the userinterface of mos
	fmt.Println("---------------------------------------- Testing UserResource ----------------------------------------")
	testUserResource()

	//test the store-resti-interface
	fmt.Println("---------------------------------------- Testing ProductResource ----------------------------------------")
	testProductInterface()
	

}

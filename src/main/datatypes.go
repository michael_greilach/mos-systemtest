package main

type SessionId struct{
	
	SessionId string
}

type UserId struct{
	
	UserId string `json:"userId"`
}

type PasswordChangeData struct{
	
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

type PasswordForgottenData struct{
	Token string `json:"token"`
	Password string `json:"password"`
}

type Error struct{
	
	ErrorCode string `json:"errorCode"`
	Message string `json:"message"`
	
}

type LoginData struct{
	
	Username string `json:"username"`
	Password string `json:"password"`
	ClientVersion int32 `json:"clientVersion"`
	ClientId int32 `json:"clientId"`
	
}
type SuccesfulLoginData struct{
	
	SessionId string `json:"sessionId"`
	UserType string `json:"userType"`
	UserId string `json:"userId"`
	RecId string `json:"recId"`
}

type ContactData struct{
	
	EmailAdress *string `json:"emailAdress"`
	Firstname *string `json:"firstName"`
	Lastname *string `json:"lastName"`
	Street *string `json:"street"`
	StreetNumber *string `json:"streetNumber"`
	City *string `json:"city"`
	ZipCode *string `json:"zipCode"`
	PhoneNumber *string `json:"phoneNumber"`
	IsVerified *bool `json:"isVerified"`
	Postbox *string `json:"postbox"`
	Salutation *string `json:"salutation"`
}

type UserData struct{
	
	UserId string `json:"userId"`
	Username string `json:"username"`
	UserType string `json:"userType"`
	Stores string `json:"stores"`
	Newsletter bool `json:"Newsletter"`
	RecId string `json:"recId"`
	Contact ContactData `json:"contact"`
}

type RegUser struct{
	
	Username string `json:"username"`
	Password string `json:"password"`
	Contact ContactData `json:"contact"`
	NewsLetter bool `json:"newsletter"`
	IsCustomer bool `json:"isCustomer"`
	//Store RegStore `json:store`
	
}

type GPSCoordinates struct{
	
	Longitude float64 `json:"longitude"`
	Latitude float64 `json:"latitude"`
	Altitude float64 `json:"altitude"`
	
}

type RegStore struct{
	
	Coords GPSCoordinates `json:"coords"`
	Company string `json:"company"`
	StoreName string `json:"storeName"`
	StoreType string `json:"storeType"`
	City string `json:"city"`
	ZipCode string `json:"zipCode"`
	Street string `json:"street"`
	StreetNumber string `json:"streetNumber"`
	
}

type NewFoodProduct struct{
	
	GtinCode  string `json:"gtinCode"`
	Caption  string `json:"caption"`
	Description  string `json:"description"`
	PriceTag  float64 `json:"priceTag"`
	Currency  string `json:"currency"`
	Producer  string `json:"producer"`
	BrandName  string `json:"brandName"`
	FoodCategory string `json:"foodCategory"`
	Flags  int32 `json:"flags"`
	
}


type FoodProduct struct{
	
	Id  string `json:"id"`
	GtinCode  string `json:"gtinCode"`
	Caption  string `json:"caption"`
	Description  string `json:"description"`
	PriceTag  float64 `json:"priceTag"`
	Currency  string `json:"currency"`
	Producer  string `json:"producer"`
	BrandName  string `json:"brandName"`
	FoodCategory string `json:"foodCategory"`
	Flags  int32 `json:"flags"`
	
}